import React, { Component } from 'react'
import Item from './Item'

export default class ListShope extends Component {
  render() {
    return (
      <div>
        <div className="row">
           {
            this.props.list.map((item) => {
                return <Item themGioHang={this.props.them} shoe={item}></Item>
            })
           }
        </div>
      </div>
    )
  }
}
